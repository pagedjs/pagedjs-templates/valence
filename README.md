---
author: Kiara Jouhanneau | @kajou.design | kajou.design@gmail.com | ko-fi.com/kajoudesign    
title: Valence  
project: paged.js sprint
book format: letter
book orientation: portrait
version: v1.0
---
  
## → fonts/type designers :  
### Movement by [María Ramos & Noel Pretorius](https://www.nmtype.com/movement/)  
>*Movement is a variable font created by Noel Pretorius and María Ramos. The typeface is inspired by dance movements. The project began as a collaboration with [Design Indaba](https://www.designindaba.com) and the contemporary South African dancer Andile Vellem. The variable font has four design extremes, Direct Black, Direct Thin, Indirect Black and Indirect Thin. The variants in-between represent the changes of movement in time. Movement is available to download and use for free.*  
### Authentic Sans by [Christina Janus & Desmond Wong](https://authentic.website)  
>*AUTHENTIC Sans explores the semiotic and aesthetic idiosyncrasies of the anonymous Latin glyphs included with CJK system fonts; the typeface aims to subvert the Eurocentric standards of typographic quality and refinement. Distributed freely under the WTFPL, AUTHENTIC Sans is a reflection of the studio praxis: expanding and redefining the visual and cultural boundaries of default systems.*
### IA Writer Mono by [Information Architects Inc.](https://github.com/iaolo/iA-Fonts)  
>*iA Writer comes bundled with iA Writer for Mac and iOS.This is a modification of IBM's Plex font. The upstream project is [here](https://github.com/IBM/plex)*
    

## → the following classes :  
>> **Component Basis**  
component-body  
component-front  
component-title  
content-opener-image  
decoration  
paragraph  

>> **Component: Long**  
biography  
case-study  
feature   
long  

>> **Component: Shortboxes**  
note  
reminder  
short  
tip  
warning  

>> **Features: Closing**  
key-term  
key-terms  
references  
review-activity  
summary  

>> **Page: Chapter**  
chapter  
chapter-number  
focus-questions  
introduction  
learning-objectives  
outline  
outline-remake  

>> **Page: Part**  
part  
part-number  
outline  


>> **Page: TOC**  
ct  
toc   
toc-body  
toc-chapter  
toc-part  
name  

>> **Others**    
restart-numbering  
running-left  
running-right  
start-right  

